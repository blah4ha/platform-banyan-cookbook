#!/usr/bin/env bash

if [ "$1" = "-h" ]; then
        echo "Usage: start-shield-ca.sh <genkey> where genkey should be set to true only the first time and empty thereafter"
        exit 0
fi

######## Update these values #########

# Get orgid from UI under settings
ORG_ID=33cbd55d-11a9-405e-b004-2c5c6a4eba41
# Examples: kubernetes, marathon, docker, none
CM=none

# If k8s, this needs to match the cluster of interest from kubeconfig
CNAME=non-prod

# Using templates.default as prod
BANYAN_URL=https://net.banyanops.com:443/api_server_host_v1

# If using marathon, set to true: marathon nodes use a custom version of openssl that seems to be causing problems
TLSNOVERIFY=false

# Examples: stage, prod, test
GROUPTYPE=non-prod

# Values: no, unless-stopped
RESTARTPOLICY=unless-stopped

######## UNTIL HERE #########


# Deploy Intermediate CA
./deploy-ca.sh $1

# wait for things to stabilize
sleep 5

# Deploy shield
./deploy-shield.sh ${ORG_ID} ${CM} ${CNAME} ${BANYAN_URL} ${TLSNOVERIFY} ${GROUPTYPE} ${RESTARTPOLICY}