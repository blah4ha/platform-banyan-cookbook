#
# Cookbook Name:: banyan
# Recipe:: templates.default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
if node['hostname'].include? 'banyan-shield'
  include_recipe 'banyan_shield'
elsif node['banyan']['role'] == 'client'
  include_recipe 'banyan_agent'
end
