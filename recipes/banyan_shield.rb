#
#
#
#
#

cookbook_file 'deploy-shield-ca-0.6.1.tar.gz' do
  source 'deploy-shield-ca-0.6.1.tar.gz'
  owner 'root'
  group 'root'
end

directory '/opt/banyan' do
  owner 'root'
  group 'root'
end

execute 'extract_banyan_shield' do
  cwd '/opt/banyan'
  command 'tar zxvf deploy-shield-ca-0.6.1.tar.gz'
  creates 'deploy-shield-ca-0.6.1'
  ignore_failure true
end

template '/opt/banyan/start-shield-ca.sh' do
  source 'start-shield-ca.sh'
  owner 'root'
  group 'root'
  mode '0775'
end

execute 'run_banyan_shield' do
  command '/opt/banyan/deploy-shield-ca-0.6.1/start-shield-ca.sh true'
  not_if File::exist? '/opt/banyan/firstrun'
  notifies :immediately, 'execute[first_run]'
end

file 'first_run' do
  command 'touch /opt/banyan/firstrun'
end