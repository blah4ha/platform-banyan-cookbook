cookbook_file "/root/netagent-#{node['netagent']['version']}.tar.gz" do
  source "netagent-#{node['netagent']['version']}.tar.gz"
  owner 'root'
  group 'root'
end

execute 'extract_netagent' do
  cwd '/root'
  command "tar zxvf netagent-#{node['netagent']['version']}.tar.gz"
  creates "/root/netagent-#{node['netagent']['version']}"
  not_if '[ -d /root/deploy-shield-ca ]'
  notifies :stop, 'service[netagent]', :immediately
  notifies :delete, 'file[/etc/init/netagent.conf]', :immediately
  notifies :run, 'execute[banyan-setup]', :delayed
  ignore_failure true
end

file '/etc/init/netagent.conf' do
  action :nothing
end

execute 'delete_old_netagent_tars' do
  command "find /root/ -type f -name 'netagent-*.tar.gz' ! -name 'netagent-#{node['netagent']['version']}.tar.gz' -delete"
  only_if "find /root/ -type f -name 'netagent-*.tar.gz' ! -name 'netagent-#{node['netagent']['version']}.tar.gz' | grep -q 'netagent'"
  ignore_failure true
end

execute 'delete_old_netagent_dirs' do
  command "find /root/ -type d -name 'netagent-*' ! -name 'netagent-#{node['netagent']['version']}' -exec rm -r {} +"
  only_if "find /root/ -type d -name 'netagent-*' ! -name 'netagent-#{node['netagent']['version']}' | grep -q 'netagent'"
  ignore_failure true
end

template "/root/netagent-#{node['netagent']['version']}/config-netagent" do
  source 'config-netagent'
  mode '0400'
  action :create
  notifies :run, 'execute[banyan-setup]', :delayed
  ignore_failure true
end

cookbook_file "/root/netagent-#{node['netagent']['version']}/cidrs.txt" do
  source 'cidrs.txt'
  mode '0400'
  action :create
  notifies :run, 'execute[banyan-setup]', :delayed
  ignore_failure true
end

cookbook_file "/root/netagent-#{node['netagent']['version']}/services.json" do
  source 'services.json'
  mode '0400'
  action :create
  notifies :run, 'execute[banyan-setup]', :delayed
  ignore_failure true
end

execute 'banyan-setup' do
  cwd "/root/netagent-#{node['netagent']['version']}"
  command './setup-netagent.sh'
  not_if { !::File.readlines('/etc/hostname').grep(/(jump|haproxy|nginx|gateway)/).empty? }
  action :nothing
  ignore_failure true
end

service 'netagent' do
  case node['platform']
  when 'ubuntu'
    if node['platform_version'] == '14.04'
      provider Chef::Provider::Service::Upstart
    else
      provider Chef::Provider::Service::Systemd
    end
  end
  action [:enable, :start]
  not_if { !::File.readlines('/etc/hostname').grep(/(jump|haproxy|nginx|gateway)/).empty? }
  not_if { node['environment'] != 'loadtest' }
  ignore_failure true
end