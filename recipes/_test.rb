# test recipe to create dummy files for inspec test. Do not use.

file '/root/netagent-0.0.1.tar.gz' do
  content 'test file'
end

directory '/root/netagent-0.0.1'

# Create dummy file inside directory so the dir is not empty (needs a recursive delete)
file '/root/netagent-0.0.1/dummy' do
  content 'test file'
end

include_recipe 'banyan::templates.default'