# # encoding: utf-8

# Inspec test for recipe node_exporter::templates.default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe service('netagent') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

# Ensure old tars are cleaned up
describe file('/root/netagent-0.0.1.tar.gz') do
  it { should_not exist }
end

# Ensure old dirs are cleaned up
describe directory('/root/netagent-0.0.1') do
  it { should_not exist }
end
